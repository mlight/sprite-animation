//
//  Frame.h
//  TestTaskProject
//
//  Created by Max Ovtsin on 13.03.15.
//  Copyright (c) 2015 Max. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MLSprite : NSObject

@property (assign, nonatomic) CGRect rect;
@property (assign, nonatomic) CGPoint offset;
@property (assign, nonatomic) CGRect sourceColorRect;
@property (assign, nonatomic) BOOL rotated;

@property (copy, nonatomic) NSString *frameName;

- (instancetype)initWithDic:(NSDictionary *)dic withFrameName:(NSString *)name;

@end
