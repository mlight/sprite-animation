//
//  ViewController.m
//  TestTaskProject
//
//  Created by Max Ovtsin on 13.03.15.
//  Copyright (c) 2015 Max. All rights reserved.
//

#import "ViewController.h"
#import "MLSpriteLayer.h"

@interface ViewController ()
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSDictionary *spriteData = [MLSpriteParser parseConfigPlistFile:@"game1_table_apple_one.plist"];
    UIImage *texture = [UIImage imageNamed:@"game1_table_apple_one.png"];
    
    MLSpriteLayer *layer = [MLSpriteLayer layerWithTexture:texture withSpritesData:spriteData];
    layer.position = CGPointMake(self.view.center.x, 200);
    [self.view.layer addSublayer:layer];
    [layer playAnimationWithFrameRate:20.0f];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
