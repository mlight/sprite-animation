//
//  MLSpriteParser.h
//  TestTaskProject
//
//  Created by Max Ovtsin on 15.03.15.
//  Copyright (c) 2015 Max. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MLSpriteParser : NSObject

+ (NSDictionary *)parseConfigPlistFile:(NSString *)fileName;

@end
