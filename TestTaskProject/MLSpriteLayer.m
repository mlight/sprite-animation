//
//  MLSpriteLayer.m
//  TestTaskProject
//
//  Created by Max Ovtsin on 15.03.15.
//  Copyright (c) 2015 Max. All rights reserved.
//

#import "MLSpriteLayer.h"

@interface MLSpriteLayer ()
@property (strong, nonatomic) UIImage *texture;
@property (strong, nonatomic) NSDictionary *data;
@end

@implementation MLSpriteLayer

- (instancetype)initWithTexture:(UIImage *)texture withSpriteData:(NSDictionary *)data {
    self = [super init];
    if (self) {
        self.contents = (__bridge id)texture.CGImage;
        _sampleIndex = 1;
        
        _texture = texture;
        _data = data;
    }
    
    return self;
}

+ (instancetype)layerWithTexture:(UIImage *)texture withSpritesData:(NSDictionary *)data {
    return [[MLSpriteLayer alloc] initWithTexture:texture withSpriteData:data];
}

- (NSUInteger)currentSampleIndex {
    return ((MLSpriteLayer *)[self presentationLayer]).sampleIndex;
}

- (void)playAnimationWithFrameRate:(float)frameRate {
    CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"sampleIndex"];
    anim.fromValue = @(1);
    anim.toValue = @(_data.count);
    anim.duration = _data.count/frameRate;
    anim.repeatCount = HUGE_VALF;
    
    [self addAnimation:anim forKey:@"sampleIndex"];
}

+ (id<CAAction>)defaultActionForKey:(NSString *)aKey {
    if ([aKey isEqualToString:@"contentsRect"] || [aKey isEqualToString:@"bounds"] || [aKey isEqualToString:@"transform"]) return (id <CAAction>)[NSNull null];
    
    return [super defaultActionForKey:aKey];
}

+ (BOOL)needsDisplayForKey:(NSString *)key {
    return [key isEqualToString:@"sampleIndex"];
}


- (void)display {
    
    NSString *key = [NSString stringWithFormat:@"game1_table_apple_one%04lu.png", (unsigned long)[self currentSampleIndex]];
    MLSprite *sprite = _data[key];
    
    if (sprite.rotated) {
        self.affineTransform = CGAffineTransformMakeRotation(M_PI * 3/2);
    } else {
        self.affineTransform = CGAffineTransformMakeRotation(0);
    }
    
    self.bounds = CGRectMake(0, 0, sprite.rect.size.width/2, sprite.rect.size.height/2);
    self.contentsRect = CGRectMake(sprite.rect.origin.x/CGImageGetWidth(_texture.CGImage),
                                   sprite.rect.origin.y/CGImageGetHeight(_texture.CGImage),
                                   sprite.rect.size.width/CGImageGetWidth(_texture.CGImage),
                                   sprite.rect.size.height/CGImageGetHeight(_texture.CGImage));
}

@end
