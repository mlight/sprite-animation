//
//  MLSpriteParser.m
//  TestTaskProject
//
//  Created by Max Ovtsin on 15.03.15.
//  Copyright (c) 2015 Max. All rights reserved.
//

#import "MLSpriteParser.h"
#import "MLSprite.h"

@implementation MLSpriteParser

+ (NSDictionary *)parseConfigPlistFile:(NSString *)fileName {
    NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:nil];
    NSData *plistData = [NSData dataWithContentsOfFile:path];
    
    NSError *error;
    NSPropertyListFormat plistFormat;
    NSDictionary *temp = [NSPropertyListSerialization propertyListWithData:plistData options:NSPropertyListImmutable format:&plistFormat error:&error];
    
    if (error) return nil;
    
    NSMutableDictionary *framesDic = [NSMutableDictionary new];
    
    for (NSString *name in temp[@"frames"]) {
        MLSprite *frame = [[MLSprite alloc] initWithDic:temp[@"frames"][name] withFrameName:name];
        [framesDic setObject:frame forKey:frame.frameName];
    }
    
    return framesDic;
}

@end
