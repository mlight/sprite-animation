//
//  MLSpriteLayer.h
//  TestTaskProject
//
//  Created by Max Ovtsin on 15.03.15.
//  Copyright (c) 2015 Max. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

#import "MLSpriteParser.h"
#import "MLSprite.h"

@interface MLSpriteLayer : CALayer

@property (assign, nonatomic) NSUInteger sampleIndex;

+ (instancetype)layerWithTexture:(UIImage *)texture withSpritesData:(NSDictionary *)data;
- (void)playAnimationWithFrameRate:(float)frameRate;

@end
