//
//  Frame.m
//  TestTaskProject
//
//  Created by Max Ovtsin on 13.03.15.
//  Copyright (c) 2015 Max. All rights reserved.
//

#import "MLSprite.h"

@implementation MLSprite

- (instancetype)initWithDic:(NSDictionary *)dic withFrameName:(NSString *)name {
    self = [super init];
    if (self) {

        _frameName = name;
        
        _rect = CGRectFromString(dic[@"frame"]);
        _offset = CGPointFromString(dic[@"offset"]);
        _sourceColorRect = CGRectFromString(dic[@"sourceColorRect"]);
        _rotated = [dic[@"rotated"] boolValue];
        
        if (_rotated) {
            CGRect tmpRect = _rect;
            _rect.size = CGSizeMake(tmpRect.size.height, tmpRect.size.width);
        }
        
    }
    
    return self;
}


@end
